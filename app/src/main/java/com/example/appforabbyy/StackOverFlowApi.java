package com.example.appforabbyy;

import retrofit2.Call;
import retrofit2.http.GET;

//Интерфейс с запросом для StackOverFlow
public interface StackOverFlowApi {
    @GET("questions?pagesize=100&order=desc&sort=activity&tagged=android&site=stackoverflow")
    Call<ListOfItems> getListOfItems();
}
